package com.example.emergencycall.test;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.NotificationManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;
import android.widget.EditText;

import com.example.emergencycall.assisted_person.EmergencyCallActivity;
import com.example.emergencycall.assisted_person.Home_assistedPerson;
import com.example.emergencycall.contact.AddContact;
import com.example.emergencycall.contact.ContactListActivity;
import com.example.emergencycall.interfaces.ContactManageInterface;
import com.example.emergencycall.interfaces.ParseActions;
import com.example.emergencycall.messenger.Messenger;
import com.example.emergencycall.messenger.MessengerReceiver;
import com.jayway.android.robotium.solo.Solo;

public class MessengerReiceiverTest extends ActivityInstrumentationTestCase2<EmergencyCallActivity> {

	private Solo solo;
	MessengerReceiver messageReceiver;

	public MessengerReiceiverTest() {
		super(EmergencyCallActivity.class);
	}
	
	@Override 
	protected void setUp() throws Exception {
		super.setUp();
		
		solo = new Solo(getInstrumentation(), getActivity());
		messageReceiver = new MessengerReceiver();

		IntentFilter i = new IntentFilter();
		i.addAction(ParseActions.emergency);

		solo.getCurrentActivity().registerReceiver(messageReceiver, i);
	}
	
	@MediumTest
	protected void testReceiver() throws JSONException {
		
		EmergencyCallActivity call = new EmergencyCallActivity();
		Messenger messenger = new Messenger();
		ContactManageInterface contactManage = new AddContact();
		TestReceiver mReceiver = new TestReceiver();
		
		this.login();
		
		// test sendMessage method in messenger.
		call.makeEmergencyCall(getActivity(), contactManage, messenger, ContactListActivity.contactsList);
		
		/*Intent intent = getActivity().getIntent();
		String action = intent.getAction();
		JSONObject json = new JSONObject(intent.getExtras().getString(
				"com.parse.Data"));

		Log.d("Test Receiver", "got action " + action + " with:");

		String username = json.getString("username");
		String message = json.getString("alert").trim().split("\n")[1];

		if (action.equals(ParseActions.message)) {
			if (username != null && message != null) {

					
			}
		}*/
		
		
	}
	
	// testing codes for login
	public void login() {

		// enter username
		EditText username = (EditText) solo
				.getView(com.example.emergencycall.R.id.login_userName);
		solo.clearEditText(username);
		// solo.enterText(username, nameStr);
		// assertTrue(solo.searchText(nameStr));
		solo.enterText(username, "terryshen");

		// enter password
		EditText password = (EditText) solo
				.getView(com.example.emergencycall.R.id.password);
		solo.clearEditText(password);
		// solo.enterText(password, passwordStr);
		// assertTrue(solo.searchText(passwordStr));
		solo.enterText(password, "123456789");

		// click remember me
		// solo.clickOnCheckBox(0);

		// click log in
		solo.clickOnButton("Sign in");

		// wait for home view opened
		solo.waitForActivity(Home_assistedPerson.class, 4000);
		solo.assertCurrentActivity("open Home Assisted Person View",
				Home_assistedPerson.class);
	}
	
	
	@Override 
	protected void tearDown() throws Exception {
		super.tearDown();
		
		solo.getCurrentActivity().unregisterReceiver(messageReceiver);
		
	}
	

}

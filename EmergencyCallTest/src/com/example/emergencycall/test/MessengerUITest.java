package com.example.emergencycall.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.emergencycall.contact.SelectContactActivity;
import com.example.emergencycall.messenger.Messenger;
import com.jayway.android.robotium.solo.Solo;

/**
 * @author Litao Shen.
 * 
 */
public class MessengerUITest extends ActivityInstrumentationTestCase2<Messenger> {

	private Solo solo;

	public MessengerUITest() {
		super(Messenger.class);
	}

	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());
	}
	
	@SmallTest
	public void testLayout() {

		assertNotNull(solo.getView(com.example.emergencycall.R.id.input));
		assertNotNull(solo.getView(com.example.emergencycall.R.id.btn_ap_statusUpdate));
		assertNotNull(solo.getView(com.example.emergencycall.R.id.screen));
		assertNotNull(solo.getView(com.example.emergencycall.R.id.txt_statusUpdate));

		EditText input = (EditText) solo.getView(com.example.emergencycall.R.id.input);
		Button send = (Button) solo.getView(com.example.emergencycall.R.id.btn_ap_statusUpdate);
		TextView infor = (TextView) solo.getView(com.example.emergencycall.R.id.txt_statusUpdate);

		assertEquals("Incorrect label of the input box", "message",
				input.getHint());
		assertEquals("Incorrect label of the send button", "send",
				send.getTag());
		assertEquals("Incorrect label of the information", "How are you feeling today?",
				infor.getText());
	}
	

	protected void tearDown() throws Exception {
		super.tearDown();
	}

}

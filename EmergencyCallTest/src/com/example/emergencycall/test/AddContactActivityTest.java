package com.example.emergencycall.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;
import android.widget.EditText;

import com.example.emergencycall.contact.AddContact;
import com.jayway.android.robotium.solo.Solo;

/**
 * @author Litao Shen.
 * 
 */
public class AddContactActivityTest extends
		ActivityInstrumentationTestCase2<AddContact> {

	private int addContactId;
	private int cancelId;
	private int inputNameId;

	private final String addContactButton = "Add";
	private final String cancelButton = "Cancel";
	private final String inputBox = "user name";

	private Solo solo;

	public AddContactActivityTest() {
		// TODO Auto-generated constructor stub
		super(AddContact.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());
	}

	@SmallTest
	public void testLayout() {

		addContactId = com.example.emergencycall.R.id.addContact;
		cancelId = com.example.emergencycall.R.id.cancel;
		inputNameId = com.example.emergencycall.R.id.newContactUsername;
		assertNotNull(solo.getView(addContactId));
		assertNotNull(solo.getView(cancelId));
		assertNotNull(solo.getView(inputNameId));
		Button addContact = (Button) solo.getView(addContactId);
		Button cancel = (Button) solo.getView((cancelId));
		EditText newContactUsername = (EditText) solo.getView(inputNameId);
		assertEquals("Incorrect label of the button", addContactButton,
				addContact.getText());
		assertEquals("Incorrect label of the button2", cancelButton,
				cancel.getText());
		assertEquals("Incorrect label of the EditText", inputBox,
				newContactUsername.getHint());
	}


	@SmallTest
	public void testCancel() {
		cancelId = com.example.emergencycall.R.id.cancel;
		Button cancel = (Button) solo.getView(cancelId);
		assertNotNull("Button not allowed to be null", cancel);

		solo.clickOnView(cancel);
		
		solo.getCurrentActivity().finish();
		solo.assertCurrentActivity("The current activity is " + solo.getCurrentActivity(),AddContact.class);
	}

	@SmallTest
	public void testInputUsername() {

		int input = com.example.emergencycall.R.id.newContactUsername;
		final EditText inputUsername = (EditText) solo.getView(input);
		assertNotNull("Button not allowed to be null", inputUsername);
		// Send string input value
		solo.clearEditText(inputUsername);
		solo.enterText(inputUsername, "terryshen");

		assertTrue(solo.searchText("terryshen"));
		
		//solo.clickOnButton(addContactButton);
	}

	@Override
	protected void tearDown() throws Exception {

		super.tearDown();
	}
}
package com.example.emergencycall.test;

import junit.framework.TestCase;

import android.content.Intent;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;

import com.example.emergencycall.assisted_person.Home_assistedPerson;
import com.example.emergencycall.carer.Home_carer;

/**
 * @author Litao Shen.
 * 
 */
public class Home_carerTest extends
    android.test.ActivityUnitTestCase<Home_carer> {

  private int buttonId;
  private int buttonId2;
  private Home_carer activity;

  public Home_carerTest() {
    super(Home_carer.class);
  }
  @Override
  protected void setUp() throws Exception {
    super.setUp();
    Intent intent = new Intent(getInstrumentation().getTargetContext(),
        Home_carer.class);
    startActivity(intent, null, null);
    activity = getActivity();
  }

  @SmallTest
  public void testLayout() {
    
    buttonId = com.example.emergencycall.R.id.assistedPerson_manage;
    buttonId2 = com.example.emergencycall.R.id.btn_carerMessages;
    assertNotNull(activity.findViewById(buttonId));
    assertNotNull(activity.findViewById(buttonId2));
    Button assistedPerson_manage = (Button) activity.findViewById(buttonId);
    Button btn_carerMessage = (Button) activity.findViewById((buttonId2));
    assertEquals("Incorrect label of the button", "My Assisted People", assistedPerson_manage.getTag());
    assertEquals("Incorrect label of the button2", "Messenger", btn_carerMessage.getTag());
  }

  @SmallTest
  public void testIntentTriggerViaOpenAssistedList() throws InterruptedException {
    buttonId = com.example.emergencycall.R.id.assistedPerson_manage;
    Button assisted_manage = (Button) activity.findViewById(buttonId);
    assertNotNull("Button not allowed to be null", assisted_manage);

    // You would call the method directly via
    getActivity().openAssistedPersonList(assisted_manage);
    
    // Check the intent which was started
    Intent triggeredIntent = getStartedActivityIntent();
    assertNotNull("Intent was null", triggeredIntent);
    String id = triggeredIntent.getExtras().getString("ID");

    assertEquals("Incorrect id passed via the intent",
        "AssistedPerson_manage", id);
  }
  
  @SmallTest
  public void testIntentTriggerViaOpenCarerMessage() {
	    buttonId2 = com.example.emergencycall.R.id.btn_carerMessages;
	    Button btn_carerMessage = (Button) activity.findViewById(buttonId2);
	    assertNotNull("Button not allowed to be null", btn_carerMessage);

	    // You would call the method directly via
	    getActivity().openCarerMessages(btn_carerMessage);
	    
	    // Check the intent which was started
	    Intent intent = getStartedActivityIntent();
	    assertNotNull("Intent was null", intent);
	    String openCarerMessageId = intent.getExtras().getString("ID");

	    assertEquals("Incorrect id passed via the intent",
	        "CarerMessages", openCarerMessageId);
  }

  @Override
  protected void tearDown() throws Exception {
    
    super.tearDown();
  }
} 

package com.example.emergencycall.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.emergencycall.assisted_person.Home_assistedPerson;
import com.example.emergencycall.contact.AddContact;
import com.example.emergencycall.contact.ContactListActivity;
import com.example.emergencycall.login.RegisterActivity;
import com.jayway.android.robotium.solo.Solo;


/**
 * @author Litao Shen.
 * 
 */
@SuppressWarnings("unchecked")
public class LoginToAddingContactTest extends ActivityInstrumentationTestCase2 {

	private final static String TAG = "AppTEST";
	private Solo solo;
	private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.example.emergencycall.login.LoginActivity";

	public static final String nameStr = "assistedperson";
	public static final String emailStr = "litaos@student.unimelb.edu.au";
	public static final String passwordStr = "123456";
	public static final String phoneStr = "1234567";

	private static Class launcherActivityClass;
	static {

		try {
			launcherActivityClass = Class
					.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
		} catch (ClassNotFoundException e) {
			Log.d(TAG, "class not found" + e.getMessage());
			throw new RuntimeException(e);

		}
	}

	public LoginToAddingContactTest() throws ClassNotFoundException {
		super(launcherActivityClass);
	}

	@Override
	protected void setUp() throws Exception {

		solo = new Solo(getInstrumentation(), getActivity());
	}

	@MediumTest
	public void testSequences() {

		// test register
		// this.register();

		// test log in
		this.login();

		// test add contact
		this.addContact();

	}

	// testing codes for register
	public void register() {

		TextView registerLink = (TextView) solo
				.getView(com.example.emergencycall.R.id.link_to_register);
		solo.clickOnView(registerLink);

		// wait for register view opened
		assertTrue(solo.waitForActivity(RegisterActivity.class, 2000));

		// choose position as assisted person
		solo.clickOnRadioButton(1);

		// enter username
		EditText username = (EditText) solo
				.getView(com.example.emergencycall.R.id.reg_fullname);
		solo.clearEditText(username);
		solo.enterText(username, nameStr);
		assertTrue(solo.searchText(nameStr));

		// enter email
		EditText email = (EditText) solo
				.getView(com.example.emergencycall.R.id.reg_email);
		solo.clearEditText(email);
		solo.enterText(email, emailStr);
		assertTrue(solo.searchText(emailStr));

		// enter phone
		EditText phone = (EditText) solo
				.getView(com.example.emergencycall.R.id.reg_phone);
		solo.clearEditText(phone);
		solo.enterText(phone, phoneStr);
		assertTrue(solo.searchText(phoneStr));

		// enter password
		EditText pw = (EditText) solo
				.getView(com.example.emergencycall.R.id.reg_password);
		solo.clearEditText(pw);
		solo.enterText(pw, passwordStr);
		assertTrue(solo.searchText(passwordStr));

		// click register
		Button register = (Button) solo
				.getView(com.example.emergencycall.R.id.btnRegister);
		assertNotNull(register);
		solo.clickOnView(register);

		assertTrue(solo.waitForActivity(launcherActivityClass, 3000));

	}

	// testing codes for login
	public void login() {

		// enter username
		EditText username = (EditText) solo
				.getView(com.example.emergencycall.R.id.login_userName);
		solo.clearEditText(username);
		// solo.enterText(username, nameStr);
		// assertTrue(solo.searchText(nameStr));
		solo.enterText(username, "terryshen");

		// enter password
		EditText password = (EditText) solo
				.getView(com.example.emergencycall.R.id.password);
		solo.clearEditText(password);
		// solo.enterText(password, passwordStr);
		// assertTrue(solo.searchText(passwordStr));
		solo.enterText(password, "123456789");

		// click remember me
		// solo.clickOnCheckBox(0);

		// click log in
		solo.clickOnButton("Sign in");

		// wait for home view opened
		solo.waitForActivity(Home_assistedPerson.class, 4000);
		solo.assertCurrentActivity("open Home Assisted Person View",
				Home_assistedPerson.class);
	}

	// test Contact list
	public void addContact() {

		Button contacts = (Button) solo
				.getView(com.example.emergencycall.R.id.carer_manage);
		assertNotNull(contacts);
		solo.clickOnView(contacts);

		// click on add new contact button
		// solo.clickOnMenuItem("Adding Contact");
		View addNewContact = (View) solo
				.getView(com.example.emergencycall.R.id.action_new);
		assertNotNull(addNewContact);
		solo.clickOnView(addNewContact);

		// wait for AddContactActivity
		solo.waitForActivity(AddContact.class, 2000);
		solo.assertCurrentActivity("open AddContact activity", AddContact.class);

		// input for new Contact username
		int input = com.example.emergencycall.R.id.newContactUsername;
		final EditText inputUsername = (EditText) solo.getView(input);
		assertNotNull("Button not allowed to be null", inputUsername);

		// Send string input value
		solo.clearEditText(inputUsername);
		solo.enterText(inputUsername, "super");

		assertTrue(solo.searchText("super"));

		// click on add
		solo.clickOnButton("Add");

		if (solo.waitForActivity(ContactListActivity.class, 3000)) {
			solo.assertCurrentActivity("return to ContactListActivity",
					ContactListActivity.class);
		} else if (solo.waitForLogMessage(
				"Contact already exists in your list", 2000)) {

			assertTrue(true);
		} else {
			
			assertTrue(true);
		}

	}

	@Override
	protected void tearDown() throws Exception {
		solo.finishOpenedActivities();
	}

}

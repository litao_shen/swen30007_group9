package com.example.emergencycall.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.emergencycall.login.RegisterActivity;
import com.jayway.android.robotium.solo.Solo;

import junit.framework.TestCase;

public class RegisterTest extends ActivityInstrumentationTestCase2 {
	
	private final static String TAG = "AppTEST";
	private Solo solo;
	private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.example.emergencycall.login.LoginActivity";

	public static final String nameStr = "assistedperson";
	public static final String emailStr = "litaos@student.unimelb.edu.au";
	public static final String passwordStr = "123456";
	public static final String phoneStr = "1234567";

	private static Class launcherActivityClass;
	static {

		try {
			launcherActivityClass = Class
					.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
		} catch (ClassNotFoundException e) {
			Log.d(TAG, "class not found" + e.getMessage());
			throw new RuntimeException(e);

		}
	}

	public RegisterTest() throws ClassNotFoundException {
		super(launcherActivityClass);
	}

	@Override
	protected void setUp() throws Exception {

		solo = new Solo(getInstrumentation(), getActivity());
	}

	@MediumTest
	public void testRegister() {

		// test register
		this.register();

	}

	// testing codes for register
	public void register() {

		TextView registerLink = (TextView) solo
				.getView(com.example.emergencycall.R.id.link_to_register);
		solo.clickOnView(registerLink);

		// wait for register view opened
		assertTrue(solo.waitForActivity(RegisterActivity.class, 2000));

		// choose position as assisted person
		solo.clickOnRadioButton(1);

		// enter username
		EditText username = (EditText) solo
				.getView(com.example.emergencycall.R.id.reg_fullname);
		solo.clearEditText(username);
		solo.enterText(username, nameStr);
		assertTrue(solo.searchText(nameStr));

		// enter email
		EditText email = (EditText) solo
				.getView(com.example.emergencycall.R.id.reg_email);
		solo.clearEditText(email);
		solo.enterText(email, emailStr);
		assertTrue(solo.searchText(emailStr));

		// enter phone
		EditText phone = (EditText) solo
				.getView(com.example.emergencycall.R.id.reg_phone);
		solo.clearEditText(phone);
		solo.enterText(phone, phoneStr);
		assertTrue(solo.searchText(phoneStr));

		// enter password
		EditText pw = (EditText) solo
				.getView(com.example.emergencycall.R.id.reg_password);
		solo.clearEditText(pw);
		solo.enterText(pw, passwordStr);
		assertTrue(solo.searchText(passwordStr));

		// click register
		Button register = (Button) solo
				.getView(com.example.emergencycall.R.id.btnRegister);
		assertNotNull(register);
		solo.clickOnView(register);

		assertTrue(solo.waitForActivity(launcherActivityClass, 3000));

	}

}

package com.example.emergencycall.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.emergencycall.assisted_person.Home_assistedPerson;
import com.example.emergencycall.contact.AddContact;
import com.example.emergencycall.contact.ContactListActivity;
import com.example.emergencycall.login.RegisterActivity;
import com.jayway.android.robotium.solo.Solo;

/**
 * @author Linus Teo.
 * 
 */
@SuppressWarnings("unchecked")
public class LoginToContactMenuTest extends ActivityInstrumentationTestCase2 {

	private final static String TAG = "AppTEST";
	private Solo solo;
	private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.example.emergencycall.login.LoginActivity";

	public static final String nameStr = "assistedperson";
	public static final String emailStr = "litaos@student.unimelb.edu.au";
	public static final String passwordStr = "123456";
	public static final String phoneStr = "1234567";

	private static Class launcherActivityClass;
	static {

		try {
			launcherActivityClass = Class
					.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
		} catch (ClassNotFoundException e) {
			Log.d(TAG, "class not found" + e.getMessage());
			throw new RuntimeException(e);

		}
	}

	public LoginToContactMenuTest() throws ClassNotFoundException {
		super(launcherActivityClass);
	}

	@Override
	protected void setUp() throws Exception {

		solo = new Solo(getInstrumentation(), getActivity());
	}

	@MediumTest
	public void testSequences() {
		// test log in
		this.login();

		// test add contact
		this.addContact();
		
		//this.callContact();
		
		this.messageContact();
		
		//untested, try not to use first
		//this.deleteContact();

	}

	// testing codes for login
	public void login() {

		// enter username
		EditText username = (EditText) solo
				.getView(com.example.emergencycall.R.id.login_userName);
		solo.clearEditText(username);
		// solo.enterText(username, nameStr);
		// assertTrue(solo.searchText(nameStr));
		solo.enterText(username, "terryshen");

		// enter password
		EditText password = (EditText) solo
				.getView(com.example.emergencycall.R.id.password);
		solo.clearEditText(password);
		// solo.enterText(password, passwordStr);
		// assertTrue(solo.searchText(passwordStr));
		solo.enterText(password, "123456789");

		// click remember me
		// solo.clickOnCheckBox(0);

		// click log in
		solo.clickOnButton("Sign in");

		// wait for home view opened
		solo.waitForActivity(Home_assistedPerson.class, 4000);
		solo.assertCurrentActivity("open Home Assisted Person View",
				Home_assistedPerson.class);
	}

	// test Contact list
	public void addContact() {

		Button contacts = (Button) solo
				.getView(com.example.emergencycall.R.id.carer_manage);
		assertNotNull(contacts);
		solo.clickOnView(contacts);

		// click on add new contact button
		// solo.clickOnMenuItem("Adding Contact");
		View addNewContact = (View) solo
				.getView(com.example.emergencycall.R.id.action_new);
		solo.clickOnView(addNewContact);

		// wait for AddContactActivity
		solo.waitForActivity(AddContact.class, 2000);
		// input for new Contact username
		int input = com.example.emergencycall.R.id.newContactUsername;
		final EditText inputUsername = (EditText) solo.getView(input);

		// Send string input value
		solo.clearEditText(inputUsername);
		solo.enterText(inputUsername, "super");

		// click on add
		solo.clickOnButton("Add");
		
		solo.clickOnButton("Cancel");
	}
	public void callContact(){
		solo.clickLongOnText("super");
		
		solo.clickOnText("Call");
        //if using real device
		float x = 240;
		float y = 625;
		solo.clickOnScreen(x,y);
	}
	
	public void messageContact(){
		solo.clickLongOnText("super");
		
		solo.clickOnText("Send Message");
		
		String testMessage = "This is a test message";
		EditText msg = (EditText) solo
				.getView(com.example.emergencycall.R.id.txtMessage);
		solo.clearEditText(msg);
		solo.enterText(msg, testMessage);
		assertTrue(solo.searchText(testMessage));
	
		solo.clickOnText("Send SMS");
		solo.goBack();
	}
	
	public void deleteContact(){
		solo.clickLongOnText("super");
		
		solo.clickOnText("Delete");
		//Note 1 test should not be found
		

        boolean expected = false;   

        boolean actual = solo.searchText("super");

        //Assert that Note 1 text is not found

        assertEquals("Text super not found, contact super is found", expected, actual);
	}

	@Override
	protected void tearDown() throws Exception {
		solo.finishOpenedActivities();
	}

}

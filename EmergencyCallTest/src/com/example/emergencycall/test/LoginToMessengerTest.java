package com.example.emergencycall.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.emergencycall.assisted_person.Home_assistedPerson;
import com.example.emergencycall.contact.ContactListActivity;
import com.example.emergencycall.contact.SelectContactActivity;
import com.example.emergencycall.login.RegisterActivity;
import com.example.emergencycall.messenger.Messenger;
import com.jayway.android.robotium.solo.Solo;

/**
 * @author Litao Shen.
 * 
 */
public class LoginToMessengerTest extends ActivityInstrumentationTestCase2 {

    private final static String TAG = "AppTEST";
    private Solo solo;
    private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.example.emergencycall.login.LoginActivity";
    
    public static final String nameStr = "assistedperson";
    public static final String emailStr = "litaos@student.unimelb.edu.au";
    public static final String passwordStr = "123456";
    public static final String phoneStr = "1234567";
    
    private static Class launcherActivityClass;
    static {
            
            try {
                    launcherActivityClass = Class.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
            } catch (ClassNotFoundException e) {
                    Log.d(TAG, "class not found" + e.getMessage());
                    throw new RuntimeException(e);
                    
            }
    }

    public LoginToMessengerTest() throws ClassNotFoundException{
            super(launcherActivityClass);
    }

    @Override
    protected void setUp() throws Exception {

            solo = new Solo(getInstrumentation(), getActivity());
    }
    
    @MediumTest
    public void testSequences() {
    
            // test register
            //this.register();
            
            // test log in
            this.login();
            
            // wait for contacts been loaded
            solo.waitForLogMessage("Contacts is downloaded", 2000);
            // test messenger
            this.messenger();

            
    }
    
    // testing codes for register
    public void register() {
            
            TextView registerLink = (TextView) solo.getView(com.example.emergencycall.R.id.link_to_register);
            solo.clickOnView(registerLink);
            
            // wait for register view opened
            assertTrue(solo.waitForActivity(RegisterActivity.class, 2000));
            
            // choose position as assisted person
            solo.clickOnRadioButton(1);
            
            // enter username
            EditText username = (EditText) solo.getView(com.example.emergencycall.R.id.reg_fullname);
            solo.clearEditText(username);
            solo.enterText(username, nameStr);
            assertTrue(solo.searchText(nameStr));
            
            // enter email
            EditText email = (EditText) solo.getView(com.example.emergencycall.R.id.reg_email);
            solo.clearEditText(email);
            solo.enterText(email, emailStr);
            assertTrue(solo.searchText(emailStr));
            
            // enter phone
            EditText phone = (EditText) solo.getView(com.example.emergencycall.R.id.reg_phone);
            solo.clearEditText(phone);
            solo.enterText(phone, phoneStr);
            assertTrue(solo.searchText(phoneStr));
            
            // enter password
            EditText pw = (EditText) solo.getView(com.example.emergencycall.R.id.reg_password);
            solo.clearEditText(pw);
            solo.enterText(pw, passwordStr);
            assertTrue(solo.searchText(passwordStr));
            
            // click register
            Button register = (Button) solo.getView(com.example.emergencycall.R.id.btnRegister);
            assertNotNull(register);
            solo.clickOnView(register);
            
            assertTrue(solo.waitForActivity(launcherActivityClass, 3000));
            
    }
    
    // testing codes for login
    public void login() {
            
            // enter username
            EditText username = (EditText) solo.getView(com.example.emergencycall.R.id.login_userName);
            solo.clearEditText(username);
            //solo.enterText(username, nameStr);
            //assertTrue(solo.searchText(nameStr));
            solo.enterText(username, "terryshen");
            
            // enter password
            EditText password = (EditText) solo.getView(com.example.emergencycall.R.id.password);
            solo.clearEditText(password);
            //solo.enterText(password, passwordStr);
            //assertTrue(solo.searchText(passwordStr));
            solo.enterText(password, "123456789");
            
            // click remember me
            //solo.clickOnCheckBox(0);
            
            // click log in
            Button sign_in = (Button) solo.getView(com.example.emergencycall.R.id.sign_in_button);
            solo.clickOnView(sign_in);
            
            // wait for home view opened
            solo.waitForActivity(Home_assistedPerson.class, 4000);
            solo.assertCurrentActivity("open Home Assisted Person View", Home_assistedPerson.class);
    }
    
    // test Messenger 
    public void messenger() {
    	
            Button messenger = (Button) solo.getView(com.example.emergencycall.R.id.btn_message);
            assertNotNull(messenger);
            solo.clickOnView(messenger);
            
            solo.waitForActivity(SelectContactActivity.class, 2000);
            solo.assertCurrentActivity("open SelectContactActivity", SelectContactActivity.class);
            
            // select first contact in list
            solo.clickInList(0);
            
            // wait for Messenger Activity
            solo.waitForActivity(Messenger.class, 2000);
            solo.assertCurrentActivity("open Messenger activity", Messenger.class);
            
            // input for message 
            int input = com.example.emergencycall.R.id.input;
            final EditText inputMessage = (EditText) solo.getView(input);
            assertNotNull("Message box not allowed to be null", inputMessage);

            // Send string input value
            solo.clearEditText(inputMessage);
            solo.enterText(inputMessage, "Hello World");

            assertTrue(solo.searchText("Hello World"));
            
            // click on send
            Button send = (Button) solo.getView(com.example.emergencycall.R.id.btn_ap_statusUpdate);
            solo.clickOnView(send);
            
            // show on screen
            EditText screen = (EditText) solo.getView(com.example.emergencycall.R.id.screen);
            assertTrue(solo.searchText("Hello World"));
    }

    @Override
    protected void tearDown() throws Exception {
            solo.finishOpenedActivities();
    }
}

package com.example.emergencycall.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.emergencycall.assisted_person.CancelAlarmActivity;
import com.example.emergencycall.assisted_person.Home_assistedPerson;
import com.example.emergencycall.contact.SelectContactActivity;
import com.example.emergencycall.login.RegisterActivity;
import com.example.emergencycall.messenger.Messenger;
import com.jayway.android.robotium.solo.Solo;

import junit.framework.TestCase;

public class LoginToRaisingAlarmTest extends  ActivityInstrumentationTestCase2 {



	private final static String TAG = "AppTEST";
    private Solo solo;
    private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.example.emergencycall.login.LoginActivity";
    
    public static final String nameStr = "assistedperson";
    public static final String emailStr = "litaos@student.unimelb.edu.au";
    public static final String passwordStr = "123456";
    public static final String phoneStr = "1234567";
    
    private static Class launcherActivityClass;
    static {
            
            try {
                    launcherActivityClass = Class.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
            } catch (ClassNotFoundException e) {
                    Log.d(TAG, "class not found" + e.getMessage());
                    throw new RuntimeException(e);
                    
            }
    }

    public LoginToRaisingAlarmTest() throws ClassNotFoundException{
            super(launcherActivityClass);
    }

    @Override
    protected void setUp() throws Exception {

            solo = new Solo(getInstrumentation(), getActivity());
    }
    
    @MediumTest
    public void testSequences() {
    
            // test register
            //this.register();
            
            // test log in
            this.login();
            
            // wait for contacts been loaded
            solo.waitForLogMessage("Contacts is downloaded", 2000);
            // test messenger
            this.makecall();

            
    }
    
    
    // testing codes for login
    public void login() {
            
            // enter username
            EditText username = (EditText) solo.getView(com.example.emergencycall.R.id.login_userName);
            solo.clearEditText(username);
            //solo.enterText(username, nameStr);
            //assertTrue(solo.searchText(nameStr));
            solo.enterText(username, "terryshen");
            
            // enter password
            EditText password = (EditText) solo.getView(com.example.emergencycall.R.id.password);
            solo.clearEditText(password);
            //solo.enterText(password, passwordStr);
            //assertTrue(solo.searchText(passwordStr));
            solo.enterText(password, "123456789");
            
            // click remember me
            //solo.clickOnCheckBox(0);
            
            // click log in
            Button sign_in = (Button) solo.getView(com.example.emergencycall.R.id.sign_in_button);
            solo.clickOnView(sign_in);
            
            // wait for home view opened
            solo.waitForActivity(Home_assistedPerson.class, 4000);
            solo.assertCurrentActivity("open Home Assisted Person View", Home_assistedPerson.class);
    }
    
    // test emergency call 
    public void makecall() {
    	
            Button raiseAlarmButton = (Button) solo.getView(com.example.emergencycall.R.id.btnRaiseAlarm);
            assertNotNull(raiseAlarmButton);
            solo.clickOnView(raiseAlarmButton);
            
            solo.waitForActivity(CancelAlarmActivity.class, 2000);
            solo.assertCurrentActivity("open CancelAlarmActivity", CancelAlarmActivity.class);
            

            assertTrue(solo.waitForDialogToOpen(2000));
            assertTrue(solo.searchText("Notifying Carers..."));
            
            // click cancel
            solo.clickOnButton(0);
            
            solo.waitForActivity(Home_assistedPerson.class, 2000);
            solo.assertCurrentActivity("open Home_assistedPerson", Home_assistedPerson.class);
           
    }

    @Override
    protected void tearDown() throws Exception {
            solo.finishOpenedActivities();
    }
}

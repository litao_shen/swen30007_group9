package com.example.emergencycall.test;

import android.content.Intent;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;

import com.example.emergencycall.assisted_person.Home_assistedPerson;

public class Home_assistedPersonTest extends
		android.test.ActivityUnitTestCase<Home_assistedPerson> {

	private int buttonId;
	private int buttonId2;
	private int buttonId3;
	private Home_assistedPerson activity;

	public Home_assistedPersonTest() {
		super(Home_assistedPerson.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				Home_assistedPerson.class);
		startActivity(intent, null, null);
		activity = getActivity();
	}

	@SmallTest
	public void testLayout() {

		buttonId = com.example.emergencycall.R.id.carer_manage;
		buttonId2 = com.example.emergencycall.R.id.btnRaiseAlarm;
		buttonId3 = com.example.emergencycall.R.id.btn_message;
		assertNotNull(activity.findViewById(buttonId));
		assertNotNull(activity.findViewById(buttonId2));
		assertNotNull(activity.findViewById(buttonId3));
		Button carer_manage = (Button) activity.findViewById(buttonId);
		Button raiseAlarmBtn = (Button) activity.findViewById((buttonId2));
		Button message = (Button) activity.findViewById(buttonId3);
		assertEquals("Incorrect label of the button", "My Carers",
				carer_manage.getTag());
		assertEquals("Incorrect label of the button2", "HELP",
				raiseAlarmBtn.getTag());
		assertEquals("Incorrect label of the button", "Messenger",
				message.getTag());
	}

	@SmallTest
	public void testIntentTriggerViaOpenCarerList() throws InterruptedException {
		buttonId = com.example.emergencycall.R.id.carer_manage;
		Button carer_manage = (Button) activity.findViewById(buttonId);
		assertNotNull("Button not allowed to be null", carer_manage);

		// You would call the method directly via
		getActivity().openCarerList(carer_manage);

		// Check the intent which was started
		Intent triggeredIntent = getStartedActivityIntent();
		assertNotNull("Intent was null", triggeredIntent);
		String id = triggeredIntent.getExtras().getString("ID");

		assertEquals("Incorrect id passed via the intent", "carer_manage", id);
	}

	@SmallTest
	public void testIntentTriggerViaRaiseAlarm() {
		buttonId2 = com.example.emergencycall.R.id.btnRaiseAlarm;
		Button btnRaiseAlarm = (Button) activity.findViewById(buttonId2);
		assertNotNull("Button not allowed to be null", btnRaiseAlarm);

		// You would call the method directly via
		getActivity().raiseAlarm(btnRaiseAlarm);

		// Check the intent which was started
		Intent intent = getStartedActivityIntent();
		assertNotNull("Intent was null", intent);
		String raiseAlarmId = intent.getExtras().getString("ID");

		assertEquals("Incorrect id passed via the intent", "btnRaiseAlarm",
				raiseAlarmId);
	}

	@SmallTest
	public void testIntentTriggerViaMessage() {
		buttonId3 = com.example.emergencycall.R.id.btn_message;
		Button btn_message = (Button) activity.findViewById(buttonId3);
		assertNotNull("Button not allowed to be null", btn_message);

		// You would call the method directly via
		getActivity().open_message(btn_message);

		// Check the intent which was started
		Intent intent = getStartedActivityIntent();
		assertNotNull("Intent was null", intent);
		String btn_messageId = intent.getExtras().getString("ID");

		assertEquals("Incorrect id passed via the intent", "StatusUpdate",
				btn_messageId);
		
	}

	@Override
	protected void tearDown() throws Exception {

		super.tearDown();
	}
}
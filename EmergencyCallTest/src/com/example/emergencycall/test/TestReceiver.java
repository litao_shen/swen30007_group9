package com.example.emergencycall.test;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.emergencycall.R;
import com.example.emergencycall.assisted_person.EmergencyCallActivity;
import com.example.emergencycall.interfaces.ParseActions;

/**
 * @author litaoshen
 * 
 */
public class TestReceiver extends BroadcastReceiver {

	private static final String TAG = "Messenger";

	@Override
	public void onReceive(Context context, Intent intent) {
		
		try {
			String action = intent.getAction();
			JSONObject json = new JSONObject(intent.getExtras().getString(
					"com.parse.Data"));

			Log.d(TAG, "got action " + action + " with:");
			// test what information can be passed in.
			Iterator itr = json.keys();
			while (itr.hasNext()) {
				String key = (String) itr.next();
				Log.d(TAG, "..." + key + " => " + json.getString(key));
			}
			
			
			// handle incoming emergency call.
			if(action.equals(ParseActions.emergency)) {
				
				Intent emergency = new Intent(context.getApplicationContext(), EmergencyCallActivity.class);
				emergency.putExtra("username", json.getString("username"));
				emergency.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				
				context.getApplicationContext().startActivity(emergency);
				
			}
			// other incoming message is handled by Messenger class which
			// is set as DefaultPushCallBack for ParseService.


		} catch (JSONException e) {
			Log.d(TAG, "JSONException: " + e.getMessage());
		}
	}


}

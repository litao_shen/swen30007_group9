package com.example.emergencycall.test;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.widget.Button;

import com.example.emergencycall.R;
import com.example.emergencycall.assisted_person.CancelAlarmActivity;
import com.example.emergencycall.assisted_person.Home_assistedPerson;
import com.example.emergencycall.messenger.Messenger;

public class RaiseAlarm_btnTest extends
    ActivityInstrumentationTestCase2<Home_assistedPerson> {

  private Home_assistedPerson activity;

  public RaiseAlarm_btnTest() {
    super(Home_assistedPerson.class);
  }
  @Override
  protected void setUp() throws Exception {
    super.setUp();
    setActivityInitialTouchMode(false);
    activity = getActivity();
  }

  public void testStartCancelAlarmActivity() throws Exception {
    
    // Add monitor to check for the second activity
    ActivityMonitor monitor = getInstrumentation().addMonitor("CancelAlarm.class", null, false);

    // Find button and click it
    Button raiseAlarm = (Button) activity.findViewById(R.id.btnRaiseAlarm);
    //TouchUtils.clickView(this, raiseAlarm);
    activity.raiseAlarm(raiseAlarm);

    // To click on a click, e.g. in a listview
    // listView.getChildAt(0);

    // Wait 2 seconds for the start of the activity
    CancelAlarmActivity startedActivity = (CancelAlarmActivity) monitor
        .waitForActivityWithTimeout(2000);
    assertNotNull(startedActivity);
    
    // Search for the button "Cancel"
    Button cancelBtn = (Button) startedActivity.findViewById(R.id.btnCancelAlarm);
    
    // Check that the Button is on the screen
    /*ViewAsserts.assertOnScreen(startedActivity.getWindow().getDecorView(),
        cancelBtn);*/
    // Validate the button on the CancelAlarm view
    assertEquals("button incorrect", "Cancel Alarm", startedActivity.getTitle());
    
	// Set up an ActivityMonitor
	ActivityMonitor receiverActivityMonitor =
	        getInstrumentation().addMonitor(Messenger.class.getName(),
	        null, false);
	
	
	// Validate that ReceiverActivity is started
	TouchUtils.clickView(this, raiseAlarm);
	Messenger receiverActivity = (Messenger) 
	        receiverActivityMonitor.waitForActivityWithTimeout(2000);
	assertNotNull("ReceiverActivity is null", receiverActivity);
	assertEquals("Monitor for ReceiverActivity has not been called",
	        1, receiverActivityMonitor.getHits());
	assertEquals("Activity is of wrong type",
	        Messenger.class, receiverActivity.getClass());
	
	// Validate that ReceiverActivity has the correct data
	
    //Read the message received by ReceiverActivity
	JSONObject json;
	try {
		json = new JSONObject(getActivity().getIntent().getExtras().getString(
				"com.parse.Data"));
		
		final String receivedMessage = json.getString("alert");
		
        //Verify that received message is correct
        assertNotNull(receivedMessage);
        assertEquals("Wrong received message", "terryshen need HELP!", receivedMessage);

	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	// Remove the ActivityMonitor
	getInstrumentation().removeMonitor(receiverActivityMonitor);

	// You would call the method directly via


  }
  
  
  @Override
  protected void tearDown() throws Exception {
    
    super.tearDown();
  }

} 
# view AndroidManifest.xml #generated:21
-keep class com.example.emergencycall.EmergencyCallApplication { <init>(...); }

# view AndroidManifest.xml #generated:59
-keep class com.example.emergencycall.assisted_person.CancelAlarmActivity { <init>(...); }

# view AndroidManifest.xml #generated:108
-keep class com.example.emergencycall.assisted_person.EmergencyCallActivity { <init>(...); }

# view AndroidManifest.xml #generated:45
-keep class com.example.emergencycall.assisted_person.Home_assistedPerson { <init>(...); }

# view AndroidManifest.xml #generated:66
-keep class com.example.emergencycall.carer.Home_carer { <init>(...); }

# view AndroidManifest.xml #generated:81
-keep class com.example.emergencycall.contact.AddContact { <init>(...); }

# view AndroidManifest.xml #generated:50
-keep class com.example.emergencycall.contact.ContactListActivity { <init>(...); }

# view AndroidManifest.xml #generated:103
-keep class com.example.emergencycall.contact.SelectContactActivity { <init>(...); }

# view AndroidManifest.xml #generated:29
-keep class com.example.emergencycall.login.LoginActivity { <init>(...); }

# view AndroidManifest.xml #generated:40
-keep class com.example.emergencycall.login.RegisterActivity { <init>(...); }

# view AndroidManifest.xml #generated:71
-keep class com.example.emergencycall.messenger.Messenger { <init>(...); }

# view AndroidManifest.xml #generated:94
-keep class com.example.emergencycall.messenger.MessengerReceiver { <init>(...); }

# view AndroidManifest.xml #generated:113
-keep class com.example.emergencycall.messenger.NotificationActivity { <init>(...); }

# view AndroidManifest.xml #generated:76
-keep class com.example.emergencycall.messenger.SendSMS { <init>(...); }

# view AndroidManifest.xml #generated:88
-keep class com.parse.ParseBroadcastReceiver { <init>(...); }

# view AndroidManifest.xml #generated:86
-keep class com.parse.PushService { <init>(...); }

# onClick res/layout/activity_home_carer.xml #generated:52
-keepclassmembers class * { *** openAssistedPersonList(...); }

# onClick res/layout/activity_homeassistedperson.xml #generated:65
-keepclassmembers class * { *** openCarerList(...); }

# onClick res/layout/activity_home_carer.xml #generated:40
-keepclassmembers class * { *** openCarerMessages(...); }

# onClick res/layout/activity_homeassistedperson.xml #generated:53
-keepclassmembers class * { *** open_message(...); }

# onClick res/layout/activity_homeassistedperson.xml #generated:40
-keepclassmembers class * { *** raiseAlarm(...); }

# onClick res/layout/activity_register.xml #generated:110
-keepclassmembers class * { *** register(...); }

# onClick res/layout/cancelalarm_activity.xml #generated:6
-keepclassmembers class * { *** returnMain(...); }

# onClick res/layout/activity_messenger.xml #generated:21
-keepclassmembers class * { *** statusUpdate(...); }


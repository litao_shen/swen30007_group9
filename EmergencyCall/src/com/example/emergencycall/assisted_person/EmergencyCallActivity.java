package com.example.emergencycall.assisted_person;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

import com.example.emergencycall.EmergencyCallApplication;
import com.example.emergencycall.R;
import com.example.emergencycall.carer.Home_carer;
import com.example.emergencycall.interfaces.ContactManageInterface;
import com.example.emergencycall.interfaces.MessengerInterface;
import com.example.emergencycall.interfaces.ParseActions;
import com.example.emergencycall.messenger.InternetManage;
import com.example.emergencycall.messenger.Messenger;
import com.parse.ParseUser;

/**
 * @author Litao Shen.
 * @author Karlen Naidoo
 * @author Linus Teo
 *
 * Allows assisted person to raise an Alarm to all carers
 * 
 */
public class EmergencyCallActivity extends Activity {

	private final String TAG = "EmergencyCallActivity";

	MessengerInterface messenger = new Messenger();
	String phoneNumber;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_emergency_call);

		// if position is carer then open alarm dialog
		if (ParseUser.getCurrentUser().getString("Position").equals("Carer")) {
			this.receiveAlarmDialog(this,
					getIntent().getExtras().getString("username"));
		} else {
			finish();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.emergency_call, menu);
		return true;
	}

	/* When an Alarm request is sent, it will issue a dialog box to Carer requesting a response.
	 * It will then send a message notifying assisted person that Carer has received the alarm. 
	 */
	public void receiveAlarmDialog(final Context context,
			final String assistedUsername) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setMessage(assistedUsername + " requires assistance!\n");
		builder.setPositiveButton("Receive",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// sending feedback message to assisted person
						messenger.sendMessage(context, assistedUsername,
								ParseUser.getCurrentUser().getUsername()
										+ " is on the way.",
								ParseActions.message);
						startActivity(new Intent(getApplicationContext(), Home_carer.class));
						finish();
					}
				});
		final AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

	/**
	 * sending emergency call to every carers if there is no Internet allowed,
	 * call by phone call function
	 * 
	 * @param contactManage
	 * @param contactsList
	 */
	public void makeEmergencyCall(Context context,
			ContactManageInterface contactManage, Messenger messenger,
			List<String> contactsList) {

		if (!contactsList.isEmpty()) {
			
			// before make a call, check internet state. Called statically
			EmergencyCallApplication.internet_state = InternetManage.isNetworkConnected(context);

			for (String contactInfo : contactsList) {
				String receiver = contactManage.retrieveUsername(contactInfo);
				// Retrieve carers phone number
				phoneNumber = contactManage.retrievePhone(contactInfo);

				Log.d(TAG, contactInfo);

				if (EmergencyCallApplication.internet_state = InternetManage.isNetworkConnected(context)) {
					messenger.sendMessage(this, receiver, ParseUser
							.getCurrentUser().getUsername()
							+ " has pressed the EMERGENCY button",
							ParseActions.emergency);
				} else {
					// Make phone call through system phone
					Intent intent = new Intent(Intent.ACTION_CALL,
							Uri.parse("tel:" + phoneNumber));
					startActivity(intent);
				}
			}
		} else {
			
			Toast.makeText(context, "Adding contact first.", Toast.LENGTH_LONG).show();
		}
	}
}

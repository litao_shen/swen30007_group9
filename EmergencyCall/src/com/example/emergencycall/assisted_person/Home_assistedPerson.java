package com.example.emergencycall.assisted_person;

/**
 * @author: Litao Shen
 * 
 * Home view for assisted person. Triggers activities to commence
 * 
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.emergencycall.R;
import com.example.emergencycall.contact.AddContact;
import com.example.emergencycall.contact.ContactListActivity;
import com.example.emergencycall.contact.ParseContactsManage;
import com.example.emergencycall.contact.SelectContactActivity;
import com.example.emergencycall.database.ContactSharedPreference;
import com.example.emergencycall.interfaces.ContactManageInterface;
import com.example.emergencycall.login.LoginActivity;
import com.example.emergencycall.messenger.Messenger;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

public class Home_assistedPerson extends Activity {

	private ParseContactsManage pFunc = new ParseContactsManage();

	ContactManageInterface contactManage = new AddContact();

	EmergencyCallActivity emergencycallact = new EmergencyCallActivity();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homeassistedperson);

		// save user as the owner of device
		ParseInstallation installation = ParseInstallation
				.getCurrentInstallation();
		installation.put("owner", ParseUser.getCurrentUser());
		Log.d("Home Assisted Person", ParseUser.getCurrentUser().toString());
		installation.saveInBackground();

	}

	@Override
	protected void onStart() {
		super.onStart();

		// load contacts when activity start for assisted person
		try {
			// load contacts to SharedPreference
			pFunc.loadContact(this);
			// adding contacts to contactslist array
			ContactListActivity.contactsList = ContactSharedPreference
					.loadList("Contacts", this);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getApplicationContext(),
					"contact list couldn't be loaded", Toast.LENGTH_SHORT)
					.show();
			;
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		try {
			// load contacts to SharedPreference
			pFunc.loadContact(this);
			// adding contacts to contactslist array
			ContactListActivity.contactsList = ContactSharedPreference
					.loadList("Contacts", this);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getApplicationContext(),
					"contact list couldn't be loaded", Toast.LENGTH_SHORT)
					.show();
			;
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			pFunc.loadContact(this);
			ContactListActivity.contactsList = ContactSharedPreference
					.loadList("Contacts", this);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getApplicationContext(),
					"contact list couldn't be loaded", Toast.LENGTH_SHORT)
					.show();
			;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.action_logout:

			LoginActivity login = new LoginActivity();
			login.log_out(this);
			startActivity(new Intent(this, LoginActivity.class));
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * @param view
	 * @throws InterruptedException
	 */
	public void openCarerList(View view) {
		Intent intent = new Intent(this, ContactListActivity.class);
		intent.putExtra("ID", "carer_manage");

		// loadingMessageView.setText("loading");
		startActivity(intent);
	}

	/**
	 * open raise alarm view
	 * 
	 * @param view
	 */
	public void raiseAlarm(View view) { // Opens Cancel Screen
		Intent intent = new Intent(this, CancelAlarmActivity.class);
		intent.putExtra("ID", "btnRaiseAlarm");

		Messenger messenger = new Messenger();

		emergencycallact.makeEmergencyCall(this, contactManage, messenger,
				ContactListActivity.contactsList);

		startActivity(intent);
	}

	/**
	 * open status update
	 * 
	 * @param view
	 */
	public void open_message(View view) { 
		Intent intent = new Intent(this, SelectContactActivity.class);
		intent.putExtra("ID", "StatusUpdate");
		startActivity(intent);
	}

}

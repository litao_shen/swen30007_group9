package com.example.emergencycall.assisted_person;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.example.emergencycall.contact.AddContact;
import com.example.emergencycall.contact.ContactListActivity;
import com.example.emergencycall.interfaces.ContactManageInterface;
import com.example.emergencycall.interfaces.MessengerInterface;
import com.example.emergencycall.interfaces.ParseActions;
import com.example.emergencycall.messenger.Messenger;
import com.parse.ParseUser;

/**
 * @author Karlen Naidoo, Litao Shen.
 * Opens when Assisted Person clicks on the Raise Alarm button
 * Dialog appears and customer has option of canceling response. 
 * 
 */
public class CancelAlarmActivity extends Activity {

	
	ContactManageInterface contactManage = new AddContact();
	MessengerInterface messenger = new Messenger();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTitle("Cancel Alarm");

		// check if there is carers present on user profile
		if (ContactListActivity.contactsList.isEmpty()) {
			contactManage.ListEmptyDialog(this);
		} else {

			// Create Dialog box
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Notifying Carers...");
			builder.setPositiveButton("Cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							// get carers username's
							for (String contactInfo : ContactListActivity.contactsList) {

								String receiver = contactManage
										.retrieveUsername(contactInfo);
								// send message to carer indicating a mistake was made
								messenger.sendMessage(CancelAlarmActivity.this,
										receiver, ParseUser.getCurrentUser()
												.getUsername()
												+ " is alright now.",
										ParseActions.cancelEmergency);
							}

							finish();
						}
					});

			final AlertDialog dialog = builder.create();
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
		}
	}
}
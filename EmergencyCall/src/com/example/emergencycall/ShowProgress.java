package com.example.emergencycall;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.View;
/**
 * @author Litao Shen.
 * 
 * Shows the progress GUI and hides the login form
 *
 */
public class ShowProgress {
	
	public ShowProgress() {
		
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show, final View homeView, final View loadView, Context context) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = context.getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			loadView.setVisibility(View.VISIBLE);
			homeView.setVisibility(View.INVISIBLE);

			loadView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							loadView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});
			homeView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							homeView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			loadView.setVisibility(show ? View.VISIBLE : View.GONE);
			homeView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

}

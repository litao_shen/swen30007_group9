package com.example.emergencycall.interfaces;
/**
 * Parse Actions
 *
 */
public interface ParseActions {

	public final String request = "com.example.emergencycall.REQUEST";
	public final String approved = "om.example.emergencycall.APPROVED";
	public final String message = "com.example.emergencycall.MESSAGE";
	public final String emergency = "com.example.emergencycall.EMERGENCY";
	public final String cancelEmergency = "com.example.emergencycall.CANCEL";
}

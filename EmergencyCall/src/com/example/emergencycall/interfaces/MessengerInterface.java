package com.example.emergencycall.interfaces;

/**
 * @author Litao Shen
 * Created an interface to modularize code
 */
import org.json.JSONException;

import android.content.Context;
import android.database.Cursor;

public interface MessengerInterface {
	
	/**
	 * Showing sent message onto screen
	 * 
	 * @param username
	 * @param message
	 */
	public void showOnScreen(String username, String message);
	
	/**
	 * get history messages from local db.
	 * 
	 * @param storageHandler
	 * @param dbcur
	 */
	public void getHistoryMessage(Cursor dbcur);
	
	/**
	 * sending message between users
	 * 
	 * @param sendTo 
	 * @param message
	 * @throws JSONException
	 */
	public void sendMessage(final Context context, String receiver,
			final String message, final String action);

}

package com.example.emergencycall.interfaces;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
/**
 * @author Litao Shen.
 * Interface for managing a contact
 * 
 */
public interface ContactManageInterface {
	
	public List<String> contactctsList = new ArrayList<String>();
	
	/**
	 * Sending friend request.
	 * @param contactsList
	 */
	public void addNewContact(List<String> contactsList);
	/**
	 * Approving incoming friend request.
	 * @param context
	 * @param username the name received
	 */
	public void approveRequest(final Context context, String username);
	/**
	 * retrieve username from contact information.
	 * @param contactInfo
	 * @return
	 */
	public String retrieveUsername(String contactInfo);
	/**
	 * retrieve phone number from contact information.
	 * @param contactInfo
	 * @return
	 */
	public String retrievePhone(String contactInfo);
	
	/**
	 * delete contact from contactsList
	 * @param position
	 * @param contactsList
	 */
	public void delete(Context context, int position, List<String> contactsList);
	
	/**
	 * Showing a dialog warning user the contacts list is empty.
	 */
	public void ListEmptyDialog(Context context);

}

package com.example.emergencycall.carer;

/**
 * Author: Litao Shen
 * Home view for Carer
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emergencycall.R;
import com.example.emergencycall.ShowProgress;
import com.example.emergencycall.contact.ContactListActivity;
import com.example.emergencycall.contact.ParseContactsManage;
import com.example.emergencycall.contact.SelectContactActivity;
import com.example.emergencycall.database.ContactSharedPreference;
import com.example.emergencycall.login.LoginActivity;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

public class Home_carer extends Activity {

	private final String TAG = "Home_carer";

	private View homeView;
	private View loadView;
	private TextView loadingMessageView;
	private ShowProgress progress = new ShowProgress();
	private ParseContactsManage pFunc = new ParseContactsManage();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_carer);

		// save user as the owner of device
		ParseInstallation installation = ParseInstallation
				.getCurrentInstallation();
		installation.put("owner", ParseUser.getCurrentUser());
		Log.d("Home Carer", ParseUser.getCurrentUser().toString());
		installation.saveInBackground();

		homeView = findViewById(R.id.home_carer);
		loadView = findViewById(R.id.carer_load_status);
		loadingMessageView = (TextView) findViewById(R.id.carer_loading_status_message);

	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		// load contact when activity start for carer 
		try {
			pFunc.loadContact(this);
			ContactListActivity.contactsList = ContactSharedPreference.loadList("Contacts", this);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), "contact list couldn't be loaded", Toast.LENGTH_SHORT).show();;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.action_logout:

			LoginActivity login = new LoginActivity();
			login.log_out(this);
			startActivity(new Intent(this, LoginActivity.class));
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Opens the assisted person view whilst showing loading transition
	 * @param view
	 */
	public void openAssistedPersonList(View view) {
		Intent intent = new Intent(this, ContactListActivity.class);
		intent.putExtra("ID", "AssistedPerson_manage");

		loadingMessageView.setText("loading");
		progress.showProgress(true, homeView, loadView, this);
		startActivity(intent);
		progress.showProgress(false, homeView, loadView, this);
	}

	@Override
	public void onPause() {
		super.onPause();
		progress.showProgress(false, homeView, loadView, this);
		
		try {
			// load contacts to SharedPreference
			pFunc.loadContact(this);
			// adding contacts to contactslist array
			ContactListActivity.contactsList = ContactSharedPreference
					.loadList("Contacts", this);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getApplicationContext(),
					"contact list couldn't be loaded", Toast.LENGTH_SHORT)
					.show();
			;
		}
	}
	

	@Override
	public void onResume() {
		super.onResume();

		try {
			pFunc.loadContact(this);
			ContactListActivity.contactsList = ContactSharedPreference
					.loadList("Contacts", this);
			Log.d(TAG, "contactsList loaded");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getApplicationContext(),
					"contact list couldn't be loaded", Toast.LENGTH_SHORT)
					.show();
			;
		}
	}

	/**
	 * opens the Carer Message layout
	 * @param view
	 */
	public void openCarerMessages(View view) {
		Intent intent = new Intent(this, SelectContactActivity.class);
		intent.putExtra("ID", "CarerMessages");
		startActivity(intent);
	}

}

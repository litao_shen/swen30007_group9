package com.example.emergencycall.login;

/**
 * Created by: Litao Shen
 * 
 * Activity which displays a login screen to the user, offering registration as
 * well.
 * 
 */
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emergencycall.R;
import com.example.emergencycall.assisted_person.Home_assistedPerson;
import com.example.emergencycall.carer.Home_carer;
import com.example.emergencycall.contact.ParseContactsManage;
import com.example.emergencycall.database.ContactSharedPreference;
import com.parse.LogInCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

public class LoginActivity extends Activity {

	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_NAME = "com.example.android.authenticatordemo.extra.Name";

	public static final String PREFS_NAME = "LOGIN";
	private static final String PREF_USERNAME = "username";
	private static final String PREF_PASSWORD = "password";
	private static SharedPreferences pref;

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// Values for userName and password at the time of the login attempt.
	private String userName;
	private String mPassword;

	// UI references.
	private EditText userNameView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;
	private CheckBox rem_meView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		// track the times of app opened
		ParseAnalytics.trackAppOpened(getIntent());

		TextView registerScreen = (TextView) findViewById(R.id.link_to_register);

		// Listening to register new account link
		registerScreen.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// Switching to Register screen
				Intent i = new Intent(getApplicationContext(),
						RegisterActivity.class);
				startActivity(i);
			}
		});

		// Set up the login form.
		userName = getIntent().getStringExtra(EXTRA_NAME);
		userNameView = (EditText) findViewById(R.id.login_userName);
		userNameView.setText(userName);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		rem_meView = (CheckBox) findViewById(R.id.rememberMe);

		pref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
		userName = pref.getString(PREF_USERNAME, null);
		mPassword = pref.getString(PREF_PASSWORD, null);

		if (userName != null && mPassword != null) {
			// Prompt for username and password
			userNameView.setText(userName);
			mPasswordView.setText(mPassword);
			attemptLogin();
		}

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.action_forgot_password:

			this.startResetPwDialog(this);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		userName = userNameView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		if (TextUtils.isEmpty(userName)) {
			userNameView.setError(getString(R.string.error_field_required));
			focusView = userNameView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {

			try {
				// Simulate network access.
				ParseUser.logInInBackground(userName, mPassword,
						new LogInCallback() {
							public void done(ParseUser user, ParseException e) {

								if (user != null) {

									if (user.getBoolean("emailVerified")) {
										String position = user
												.getString("Position");

										Log.i("Position", position);
										// Hooray! The user is logged in.
										if (position.equals("AssistedPerson")) {
											startActivity(new Intent(
													getApplicationContext(),
													Home_assistedPerson.class));
										} else if (position.equals("Carer")) {
											startActivity(new Intent(
													getApplicationContext(),
													Home_carer.class));
										}

										if (rem_meView.isChecked()) {
											getSharedPreferences(PREFS_NAME,
													MODE_PRIVATE)
													.edit()
													.putString(PREF_USERNAME,
															userName)
													.putString(PREF_PASSWORD,
															mPassword).commit();
										}
									} else {
										Toast.makeText(
												getApplicationContext(),
												"Please verify your email first.",
												Toast.LENGTH_LONG).show();
									}

								} else {
									// Signup failed. Look at the ParseException
									// to see what happened.
									Log.e("login", e.getMessage());
									mPasswordView
											.setError(getString(R.string.error_incorrect_password));
									mPasswordView.requestFocus();

									Toast.makeText(getApplicationContext(),
											e.getMessage(), Toast.LENGTH_LONG)
											.show();
								}
							}
						});
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				
				Toast.makeText(getApplicationContext(),
						"Can not log in due to internet problem.", Toast.LENGTH_LONG)
						.show();
				return false;
			}

			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);
			// finish();
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

	/**
	 * allowing user to reset their password
	 * @param context
	 */
	public void startResetPwDialog(final Context context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setMessage("To reset your password please input your email address.");

		// Set an EditText view to get user input
		final EditText input = new EditText(context);
		input.requestFocus();
		input.setHint("email address");
		builder.setView(input);

		builder.setPositiveButton("Send",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						String email = input.getText().toString();

						ParseUser.requestPasswordResetInBackground(email,
								new RequestPasswordResetCallback() {
									public void done(ParseException e) {
										if (e == null) {
											// An email was successfully
											// sent with reset instructions.
										} else {
											// Something went wrong. Look at
											// the ParseException to see
											// what's up.
											Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
										}
									}
								});
					}
				});

		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
					}
				});

		final AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

	public void log_out(Context context) {
		ParseContactsManage pfunc = new ParseContactsManage();
		pfunc.log_out();
		// wipe out shared preference
		ContactSharedPreference.clearSharedPreference(context, ContactSharedPreference.PREF_NAME);
		ContactSharedPreference.clearSharedPreference(context, PREFS_NAME);
		
	}
}

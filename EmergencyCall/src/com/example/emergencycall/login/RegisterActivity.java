package com.example.emergencycall.login;
/**
 * Created by: Litao Shen
 * Modified by: Karlen Naidoo
 * 
 */

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emergencycall.R;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class RegisterActivity extends Activity {
	
	EditText nameView;
	
	EditText emailView;
	
	EditText phoneView;
	
	EditText passwordView;
	
	RadioButton carerView;
	
	RadioButton assistedView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set View to register.xml
		setContentView(R.layout.activity_register);

		TextView loginScreen = (TextView) findViewById(R.id.link_to_login);
		
		nameView = (EditText) findViewById(R.id.reg_fullname);
		
		emailView = (EditText) findViewById(R.id.reg_email);
		
		phoneView = (EditText) findViewById(R.id.reg_phone);
		
		passwordView = (EditText) findViewById(R.id.reg_password);
		
		carerView = (RadioButton) findViewById(R.id.reg_Carer);
		
		assistedView = (RadioButton) findViewById(R.id.reg_Assisted);

		// Listening to Login Screen link
		loginScreen.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				// Closing registration screen
				// Switching to Login Screen/closing register screen
				finish();
			}
		});
	}
	
	
	public void register(View view) {
		
		String name = nameView.getText().toString();
		
		String email = emailView.getText().toString();
		
		String phone = phoneView.getText().toString();
		
		String pw = passwordView.getText().toString();
		
		String position="";
		
		if(carerView.isChecked()) {
			position = carerView.getText().toString();
		} else if(assistedView.isChecked()) {
			position = assistedView.getText().toString();
		}
		
		// Reset errors.
		passwordView.setError(null);
		nameView.setError(null);
		emailView.setError(null);
		phoneView.setError(null);

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(pw)) {
			passwordView.setError(getString(R.string.error_field_required));
			focusView = passwordView;
			cancel = true;
		} else if (pw.length() < 4) {
			passwordView.setError(getString(R.string.error_invalid_password));
			focusView = passwordView;
			cancel = true;
		}
		
		// Check for a valid userName
		if (TextUtils.isEmpty(name)) {
			nameView.setError(getString(R.string.error_field_required));
			focusView = nameView;
			cancel = true;
		} else if (name.length() < 2) {
			nameView.setError(getString(R.string.error_invalid_userName));
			focusView = nameView;
			cancel = true;
		}
		
		// Check for a valid email
		if (TextUtils.isEmpty(email)) {
			emailView.setError(getString(R.string.error_field_required));
			focusView = emailView;
			cancel = true;
		} else if (! email.contains("@")) {
			emailView.setError(getString(R.string.error_invalid_email));
			focusView = emailView;
			cancel = true;
		}
		
		// Check for valid phone number
		if (TextUtils.isEmpty(phone)) {
			phoneView.setError(getString(R.string.error_field_required));
			focusView = phoneView;
			cancel = true;
		} 
		
		// Check for a valid position
		if (TextUtils.isEmpty(position)) {
			carerView.setError(getString(R.string.error_field_required));
			focusView = carerView;
			cancel = true;
		} 

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {

			ParseUser user = new ParseUser();
			user.setUsername(name);
			user.setPassword(pw);
			user.setEmail(email);

			// other fields can be set just like with ParseObject
			user.put("Position", position);
			user.put("phone", phone);

			user.signUpInBackground(new SignUpCallback() {
				public void done(ParseException e) {
					if (e == null) {
						// Hooray! Let them use the app now.
						//startActivity(new Intent(getApplicationContext(),
						//	LoginActivity.class));
						Toast.makeText(getApplicationContext(),
								"Sign up successfully. Try it now!",
								Toast.LENGTH_SHORT).show();
						finish();
						
					} else {
						// Sign up didn't succeed. Look at the ParseException
						// to figure out what went wrong
						Log.e("register", e.getMessage());
						Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
					}
				}
			});
		}
		
	}

}

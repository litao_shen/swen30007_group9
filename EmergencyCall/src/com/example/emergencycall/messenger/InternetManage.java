package com.example.emergencycall.messenger;

/**
 * @author Karlen Naidoo
 * Checks for an active network connection
 */
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class InternetManage {

	// Check for Internet connection (Permissions set in Manifest file
    // return true if network is working, otherwise false.
	public static boolean isNetworkConnected(Context context) {
		ConnectivityManager conMgr =  (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();

		if (activeNetwork != null && activeNetwork.isConnected()) {
			return true;
		    //notify user you are online
		} else {
			Toast.makeText(context, "Cannot connetct to internet.", Toast.LENGTH_SHORT).show();;
			return false;
		    //notify user you are not online
		} 
	}
}

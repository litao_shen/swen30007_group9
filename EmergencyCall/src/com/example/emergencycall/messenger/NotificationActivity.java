package com.example.emergencycall.messenger;

/**
 * @author Litao Shen
 * @contributor: Karlen Naidoo
 */
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.emergencycall.R;

public class NotificationActivity extends Activity{
	
	private static final String TAG = "NotificationActivity";
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
	    super.onCreate(savedInstanceState);
	    executor.submit(new NotificationTask());
	}

	private class NotificationTask implements Runnable {
	    @Override
	    public void run() {
	        while (!executor.isShutdown()) {
	            try {
	                String message = blockingQueue.take();
	                notification(message, NotificationActivity.this);
	                // Allow 5 seconds for every message.
	                Thread.sleep(5000);
	            } catch (InterruptedException ex) {
	                Log.e(TAG, "", ex);
	                // Error occurs. Stop immediately.
	                break;
	            }                
	        }

	    }
	}

	@Override
	public void onDestroy() {
	    // Will be triggered during back button pressed.
	    super.onDestroy();
	    executor.shutdownNow();
	    try {
	        executor.awaitTermination(100, TimeUnit.DAYS);
	    } catch (InterruptedException ex) {
	        Log.e(TAG, "", ex);
	    }
	}

	private void notification(String message, Context context) {
		
		// set attribute properties
	    NotificationCompat.Builder mBuilder =
	        new NotificationCompat.Builder(context)
	        .setSmallIcon(R.drawable.ic_launcher)
	        .setContentTitle(this.getString(R.string.app_name))
	        .setTicker(message)
	        .setContentText(message)
	        .setAutoCancel(true)
	        .setOnlyAlertOnce(true);

	    final NotificationManager mNotificationManager =
	        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	    final int id = atomicInteger.getAndIncrement();
	    mNotificationManager.notify(id, mBuilder.build());

	    Handler handler = new Handler(Looper.getMainLooper());
	    handler.postDelayed(new NotificationManagerCancelRunnable(mNotificationManager, id), 5000);
	}

	// Use static class, to avoid memory leakage.
	private static class NotificationManagerCancelRunnable implements Runnable {
	    private final NotificationManager notificationManager;
	    private final int id;

	    public NotificationManagerCancelRunnable(NotificationManager notificationManager, int id) {
	        this.notificationManager = notificationManager;
	        this.id = id;
	    }

	    @Override
	    public void run() {
	        notificationManager.cancel(id);
	    }
	}

	// For notification usage. 128 is just a magic number.
	private final ExecutorService executor = Executors.newSingleThreadExecutor();
	private final BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<String>(128);
	private final AtomicInteger atomicInteger = new AtomicInteger(0);   

}

package com.example.emergencycall.messenger;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emergencycall.R;
import com.example.emergencycall.contact.ContactApproveDialog;
import com.example.emergencycall.contact.ParseContactsManage;
import com.example.emergencycall.contact.SelectContactActivity;
import com.example.emergencycall.database.LocalStorageHandler;
import com.example.emergencycall.interfaces.MessengerInterface;
import com.example.emergencycall.interfaces.ParseActions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * @author Karlen Naidoo, Litao Shen.
 * Class responsible for displaying message history, and connects intended recipients together
 * 
 */
public class Messenger extends Activity implements MessengerInterface {

	final String TAG = "Status Update";

	// create instance of ContactApproveActivity;
	private ContactApproveDialog approveAct = new ContactApproveDialog();
	private MessageReceiver messageReceiver = new MessageReceiver();

	// Views on UI
	EditText input;
	EditText screen;
	Button send;

	// storagehandler from LocalStorageHandler class
	private LocalStorageHandler storagehandler;
	private Cursor dbcur;

	// record username that want to send message to
	String receiver;
	String currentUser;
	String message;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_messenger);

		// Make sure we're running on Honeycomb or higher to use ActionBar APIs
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// Show the Up button in the action bar.
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		input = (EditText) findViewById(R.id.input);
		input.requestFocus();
		screen = (EditText) findViewById(R.id.screen);
		send = (Button) findViewById(R.id.btn_ap_statusUpdate);

	}

	@Override
	public void onStart() {

		super.onStart();

		currentUser = ParseUser.getCurrentUser().getUsername();

		Intent request = getIntent();

		if (request.getExtras() != null) {

			if (request.getExtras().getString("username") != null) {
				receiver = request.getExtras().getString("username");
				storagehandler = new LocalStorageHandler(this);
				dbcur = storagehandler.get(receiver, currentUser);
				this.getHistoryMessage(dbcur);
				storagehandler.close();
			}

			setTitle("Message with " + receiver);

			// handle incoming intent from Parse push notification
			if (request.getExtras().getString("com.parse.Data") != null) {
				try {

					JSONObject json = new JSONObject(request.getExtras()
							.getString("com.parse.Data"));

					String action = json.getString("action");

					Log.d(TAG, "got action " + action + " with:");

					receiver = json.getString("username");

					// reload history message to screen
					storagehandler = new LocalStorageHandler(this);
					dbcur = storagehandler.get(receiver, currentUser);
					this.getHistoryMessage(dbcur);
					storagehandler.close();

					// if action is REQUEST, then start ContactApproveActivity
					if (action.equals(ParseActions.request)) {

						Log.d(TAG, receiver);
						approveAct.startApproveDialog(this, receiver);

					} else if (action.equals(ParseActions.message)) {
						message = json.getString("alert").trim().split("\n")[1];
						Log.d(TAG, message);

						if (message != null) {
							this.showOnScreen(receiver, message);
							storagehandler.insert(receiver, currentUser,
									message);
						}
					} else if (action.equals(ParseActions.approved)) {
						message = json.getString("alert");
						Log.d(TAG, message);

						ParseContactsManage pFunc = new ParseContactsManage();

						if (message != null) {

							// add user himself to other user's list
							ArrayList<String> phoneNum = new ArrayList<String>();
							phoneNum.add(ParseUser.getCurrentUser().getString(
									"phone"));
							pFunc.saveContact(this, ParseUser.getCurrentUser(),
									receiver, phoneNum, 1);

							this.showOnScreen(receiver, message);
							storagehandler.insert(receiver, currentUser,
									message);
						}

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT)
							.show();
					e.printStackTrace();
				}

				setTitle("Message with " + receiver);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		IntentFilter i = new IntentFilter();
		i.addAction(ParseActions.message);

		registerReceiver(messageReceiver, i);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.assisted_statusupdate, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			startActivity(new Intent(this, SelectContactActivity.class));
			finish();
			this.unregisterReceiver(messageReceiver);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * sending status update messages and update messages to view
	 * 
	 * @param view
	 */
	public void statusUpdate(View view) {

		if (TextUtils.isEmpty(input.getText().toString())) {
			Toast.makeText(this, "Please input message", Toast.LENGTH_SHORT)
					.show();
		} else {
			String message = input.getText().toString();
			// send message to receiver
			this.sendMessage(this, receiver, message, ParseActions.message);
			Log.d(TAG, "send message to " + receiver);

			// reset input box
			input.setText("");

			// adding message to history list and show it on screen
			showOnScreen(ParseUser.getCurrentUser().getUsername(), message);
			storagehandler.insert(currentUser, receiver, message);
		}
	}

	/**
	 * get history messages from local db.
	 * 
	 * @param storageHandler
	 * @param dbcur
	 */
	public void getHistoryMessage(Cursor dbcur) {

		if (dbcur.getCount() > 0) {
			int noOfScorer = 0;
			dbcur.moveToFirst();
			while ((!dbcur.isAfterLast()) && noOfScorer < dbcur.getCount()) {
				noOfScorer++;

				// adding message to screen from db.
				this.showOnScreen(dbcur.getString(2), dbcur.getString(3));
				dbcur.moveToNext();
			}
		}
		storagehandler.close();
	}

	/**
	 * Showing sent message onto screen
	 * 
	 * @param username
	 * @param message
	 */
	public void showOnScreen(String username, String message) {

		if (username != null && message != null) {
			screen.append(username + ":\n");
			screen.append(message + "\n");
		}
	}

	/**
	 * @author litaoshen
	 * 
	 */
	public class MessageReceiver extends BroadcastReceiver {

		public MessageReceiver() {

		}

		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				String action = intent.getAction();
				JSONObject json = new JSONObject(intent.getExtras().getString(
						"com.parse.Data"));

				Log.d(TAG, "got action " + action + " with:");

				String username = json.getString("username");
				String message = json.getString("alert").trim().split("\n")[1];

				if (action.equals(ParseActions.message)) {
					if (username != null && message != null) {

						if (receiver.equals(username)) {
							showOnScreen(username, message);
							storagehandler.insert(username, currentUser,
									message);
							((NotificationManager) context
									.getSystemService(NOTIFICATION_SERVICE))
									.cancel((username + ":\n" + message)
											.hashCode());
						}
					}
				}

			} catch (JSONException e) {
				Log.d(TAG, "JSONException: " + e.getMessage());
			}
		}

	};

	/**
	 * sending message between users
	 * 
	 * @param sendTo
	 * @param message
	 */
	public void sendMessage(final Context context, String sendTo,
			final String message, final String action) {

		if (sendTo != null) {
			ParseQuery<ParseUser> query = ParseUser.getQuery();
			query.whereEqualTo("username", sendTo);
			Log.d(TAG, sendTo);

			String s = "{\"action\": \"" + action + "\"," + "\"alert\": \""
					+ ParseUser.getCurrentUser().getUsername() + ":\n"
					+ message + "\"," + "\"username\": \""
					+ ParseUser.getCurrentUser().getUsername() + "\"" + "}";

			try {
				final JSONObject data = new JSONObject(s);

				query.findInBackground(new FindCallback<ParseUser>() {
					public void done(List<ParseUser> objects, ParseException e) {
						if (!objects.isEmpty()) {
							// The query was successful.
							// user is found
							for (ParseUser usr : objects) {

								// Create our Installation query
								ParseQuery<ParseInstallation> pushQuery = ParseInstallation
										.getQuery();

								// sending message to user which match username
								// passed
								// in.
								pushQuery.whereEqualTo("owner", usr);
								Log.d("Message",
										"found user " + usr.getUsername());

								// Send push notification to query
								ParsePush push = new ParsePush();
								push.setQuery(pushQuery); // Set our
															// Installation
															// query
								push.setData(data);
								push.sendInBackground();
							}
						} else if (e != null) {
							Toast.makeText(context, (CharSequence) e,
									Toast.LENGTH_SHORT).show();
						}
					}
				});
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

}

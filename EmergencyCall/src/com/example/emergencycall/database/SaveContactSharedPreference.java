package com.example.emergencycall.database;

/**
 * @author: Litao Shen
 * Stores the preferences of contacts within local database
 */

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;

public class SaveContactSharedPreference {
	
	public final static String PREF_NAME = "Contacts";
	
	
	public static boolean saveList(List<String> list, String listName, Context mContext) { 

		  SharedPreferences prefs = mContext.getSharedPreferences(PREF_NAME, 0);
		  SharedPreferences.Editor editor = prefs.edit();

		  editor.putInt(listName + "size", list.size());
		  for(String s: list)
		    editor.putString(listName, s);
		  return editor.commit();
		}

	public static List<String> loadList(String listName, Context mContext) {
		  SharedPreferences prefs = mContext.getSharedPreferences(PREF_NAME, 0);
		  
		  int size = prefs.getInt(listName + "size", 0);
		  List<String> contacts = new ArrayList<String>(size);

		  for(int i = 0; i < size; i ++) {
		    contacts.add(prefs.getString(listName, null));
		  }
		  return contacts;
		}
	
	public static void clearSharedPreference(Context context, String prefName) {
	
		SharedPreferences prefs = context.getSharedPreferences(prefName, 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.clear(); // remove specific shared preference
		editor.commit();
	}
}

package com.example.emergencycall.database;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;

public class ContactSharedPreference {
	
	public final static String PREF_NAME = "Contacts";
	
	
	/**
	 * Save contact list into SharedPreference
	 * @param list
	 * @param listName
	 * @param mContext
	 * @return
	 */
	public static boolean saveList(List<String> list, String listName, Context mContext) { 

		  SharedPreferences prefs = mContext.getSharedPreferences(PREF_NAME, 0);
		  SharedPreferences.Editor editor = prefs.edit();

		  editor.putInt(listName + "size", list.size());
		  for(int i = 0; i < list.size(); i ++) {
		    editor.putString(listName + i, list.get(i));
		  }
		  return editor.commit();
		}

	/**
	 * load contacts from SharedPreference
	 * @param listName
	 * @param mContext
	 * @return
	 */
	public static List<String> loadList(String listName, Context mContext) {
		  SharedPreferences prefs = mContext.getSharedPreferences(PREF_NAME, 0);
		  
		  int size = prefs.getInt(listName + "size", 0);
		  List<String> contacts = new ArrayList<String>();

		  for(int i = 0; i < size; i ++) {
		    contacts.add(prefs.getString(listName + i, ""));
		  }
		  return contacts;
		}
	
	/**
	 * Delete SharedPreference when log out.
	 * @param context
	 * @param prefName
	 */
	public static void clearSharedPreference(Context context, String prefName) {
	
		SharedPreferences prefs = context.getSharedPreferences(prefName, 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.clear(); // remove specific shared preference
		editor.commit();
	}
}

package com.example.emergencycall.contact;

/**
 * @author Litao Shen.
 * Dialog box to confirm the contacts approval
 * 
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.example.emergencycall.interfaces.ContactManageInterface;

public class ContactApproveDialog {

	static final String TAG = "Approve Contact";

	public void startApproveDialog(final Context context, final String username) {
		
		final ContactManageInterface approveContact = new AddContact();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(username + " sends you a request.").setTitle(
				"Contact Request");
		builder.setPositiveButton("approve",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// if approve button pressed, using approveRequest method to approve request.
						approveContact.approveRequest(
								context, username);
						
					}
				});

		builder.setNegativeButton("Reject",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				});

		final AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		
	}
	

}

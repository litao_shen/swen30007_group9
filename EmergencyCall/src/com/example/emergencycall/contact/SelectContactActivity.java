package com.example.emergencycall.contact;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.emergencycall.R;
import com.example.emergencycall.assisted_person.Home_assistedPerson;
import com.example.emergencycall.carer.Home_carer;
import com.example.emergencycall.interfaces.ContactManageInterface;
import com.example.emergencycall.messenger.Messenger;
import com.parse.ParseUser;
/**
 * @author Litao Shen
 * @author Karlen Naidoo
 * @author Linus Teo
 * 
 * Select the required contact using the retrieve username method
 *
 */
public class SelectContactActivity extends ListActivity {

	private final String TAG = "SelectContact";

	private ListView contactsView;
	List<String> list = new ArrayList<String>();
	private ArrayAdapter<String> listAdapter;
	private Context context;
	
	ContactManageInterface contactManage = new AddContact(); 

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.activity_select_contact);

		// contacts = (ListView)findViewById(R.id.list);
		contactsView = getListView();
		contactsView.setTextFilterEnabled(true);

		
		// Make sure we're running on Honeycomb or higher to use ActionBar APIs
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// Show the Up button in the action bar.
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		// copy all contents in contactsList into list
		list.addAll(ContactListActivity.contactsList);
		
		if(list.isEmpty()) {
			contactManage.ListEmptyDialog(this);
		}

		listAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, list);
		// binding the data to ListView
		setListAdapter(listAdapter);
		registerForContextMenu(contactsView);
		
		context = this.getApplicationContext();
		
		contactsView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {

                Intent intent = new Intent(context, Messenger.class);
                intent.putExtra("username", contactManage.retrieveUsername(list.get(position)));
                
                startActivity(intent);
                finish();
                
                Log.d(TAG, contactManage.retrieveUsername(list.get(position)));
            }
        });

	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_contact, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			String position = ParseUser.getCurrentUser().getString("Position");
			if (position.equals("AssistedPerson")) {
				startActivity(new Intent(this, Home_assistedPerson.class));
			} else if (position.equals("Carer")) {
				startActivity(new Intent(this, Home_carer.class));
			}
			
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}

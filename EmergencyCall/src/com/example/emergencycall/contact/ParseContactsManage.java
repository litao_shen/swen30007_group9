package com.example.emergencycall.contact;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.emergencycall.database.ContactSharedPreference;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;

/**
 * @author Litao Shen.
 * 
 */
public class ParseContactsManage {

	/**
	 * save contact into JSON object
	 * 
	 * @param name
	 *            of contact passed in
	 * @param contactNumber
	 *            : all contact number from contact
	 * @throws JSONException
	 */
	public void saveContact(final Context context, final ParseUser savetoUsr,
			final String name, final List<String> contactNumber,
			final int priority) {

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Contacts");
		query.whereEqualTo("Name", name);
		Log.d("Search", name);

		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> nameList, ParseException e) {
				// if contact not exist in contact list of current user.
				if (nameList.isEmpty()) {
					ParseObject pContactObj = new ParseObject("Contacts");
					pContactObj.put("Priorty", priority);
					pContactObj.put("Name", name);
					Log.d("SAVE CONTACT", name);

					if (contactNumber != null) {
						for (int i = 0; i < contactNumber.size(); i++) {
							pContactObj.put("Phone", contactNumber.get(i));
							Log.d("SAVE CONTACT", contactNumber.get(i));
						}
					}
					pContactObj.setACL(new ParseACL(savetoUsr));
					pContactObj.saveInBackground();
				} else {
					Log.d("Save Contact", name);
					Toast.makeText(context, "contact already exists.",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

	}

	/**
	 * @param newName
	 * @param newContact
	 */
	public void updateContact(ParseUser usr, final String contactNumber,
			final List<String> newContact) {

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Contacts");
		// Retrieve the object by id
		query.getInBackground(usr.getUsername(),
				new GetCallback<ParseObject>() {
					public void done(ParseObject contact, ParseException e) {
						if (e == null) {
							// Now let's update it with some new data. In this
							// case, only cheatMode and score
							// will get sent to the Parse Cloud. playerName
							// hasn't changed.
							contact.put("Number", contactNumber);
							for (int i = 0; i < newContact.size(); i++) {
								contact.put("Contact", newContact);
							}
							contact.saveInBackground();
						}
					}
				});

	}

	/**
	 * @param name
	 */
	public void pDeleteContact(Context context, String name) {

		PushService.unsubscribe(context, name
				+ ParseUser.getCurrentUser().getUsername());
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Contacts");

		query.whereEqualTo("Name", name);
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> pContactObj, ParseException e) {
				// TODO Auto-generated method stub
				if (!pContactObj.isEmpty()) {
					if (pContactObj.get(0) != null) {
						pContactObj.get(0).deleteInBackground();
					}
				}

			}
		});

	}

	/**
	 * load contacts to shared preference file.
	 * @param contactsList
	 * @throws ParseException
	 */
	public void loadContact(final Context context) throws ParseException {

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Contacts");

		query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);

		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> pContactObj, ParseException e) {
				// if contact not exist in contact list
				if (!pContactObj.isEmpty()) {

					List<String> contacts = new ArrayList<String>();

					for (ParseObject obj : pContactObj) {

						String contactInfo = "";

						contactInfo = obj.getString("Name") + "\n"
								+ obj.getString("Phone");
						Log.d("LOAD CONTACT",
								obj.getString("Name") + " "
										+ obj.getString("Phone"));

						contacts.add(contactInfo);

					}
					ContactSharedPreference.saveList(contacts, "Contacts",
							context);
					//Toast.makeText(context, "Contacts is downloaded", Toast.LENGTH_SHORT).show();

				} else {
					// remove old shared preference file
					ContactSharedPreference.clearSharedPreference(context, ContactSharedPreference.PREF_NAME);
					// Remove for demonstration 
					/*Toast.makeText(context, "Contacts List is empty",
							Toast.LENGTH_SHORT).show();*/
				}
			}
		});
	}

	/**
	 * log out user and delete his automatic log in info.
	 */
	public void log_out() {
		ParseUser.logOut();
	}

}

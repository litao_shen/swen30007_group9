package com.example.emergencycall.contact;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emergencycall.R;
import com.example.emergencycall.interfaces.ContactManageInterface;
import com.example.emergencycall.interfaces.ParseActions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * @author Litao Shen.
 * Allows users to add other users if the username is known and within the Parse database
 * 
 */
public class AddContact extends Activity implements OnClickListener,
		ContactManageInterface {

	private static Button mAddContactButton;
	private static Button mCancelButton;
	private static EditText mContactUserNameText;

	private static final String LOG_TAG = "AddContact";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_add_contact);

		mAddContactButton = (Button) findViewById(R.id.addContact);
		mCancelButton = (Button) findViewById(R.id.cancel);
		mContactUserNameText = (EditText) findViewById(R.id.newContactUsername);

		if (mAddContactButton != null) {
			mAddContactButton.setOnClickListener(this);
		} else {
			Log.e(LOG_TAG, "onCreate: mAddContactButton is null");
			throw new NullPointerException(
					"onCreate: mAddContactButton is null");
		}

		if (mCancelButton != null) {
			mCancelButton.setOnClickListener(this);
		} else {
			Log.e(LOG_TAG, "onCreate: mCancelButton is null");
			throw new NullPointerException("onCreate: mCancelButton is null");
		}
	}

	@Override
	public void onClick(View view) {
		if (view == mCancelButton) {
			finish();
		} else if (view == mAddContactButton) {

			addNewContact(ContactListActivity.contactsList);

		} else {
			Log.e(LOG_TAG, "onClick: view clicked is unknown");
		}
	}

	/**
	 * sending friend request to user.
	 * 
	 * @param contactsList
	 */
	public void addNewContact(List<String> contactsList) {

		if (contactsList.size() < 4) {
			String name = mContactUserNameText.getText().toString();
			// check user is in contacts list already.
			Boolean contain = false;
			for (String s : contactsList) {
				String n = s.trim().split("\n")[0];
				if (n.equals(name)) {
					contain = true;
				}
			}

			if (mContactUserNameText.length() > 0
					&& !name.equals(ParseUser.getCurrentUser().getUsername())) {
				// not already inside contact list
				if (!contain) {

					ParseQuery<ParseUser> query = ParseUser.getQuery();
					query.whereEqualTo("username", mContactUserNameText
							.getText().toString());
					Log.d(LOG_TAG, mContactUserNameText.getText().toString());

					query.findInBackground(new FindCallback<ParseUser>() {
						public void done(List<ParseUser> objects,
								ParseException e) {
							if (!objects.isEmpty()) {
								// The query was successful.
								for (ParseUser usr : objects) {
									
									/* check current user is not with same position of user
									 * that he wants to add.
									 */
									if (!usr.getString("Position").equals(
											ParseUser.getCurrentUser()
													.getString("Position"))) {
										String s = "{\"action\": \"com.example.emergencycall.REQUEST\","
												+ "\"alert\": \"Friend Request from "
												+ ParseUser.getCurrentUser()
														.getUsername()
												+ "!\","
												+ "\"username\": \""
												+ ParseUser.getCurrentUser()
														.getUsername()
												+ "\""
												+ "}";

										// sending request to user.
										try {
											JSONObject data = new JSONObject(s);

											// Create our Installation query
											ParseQuery<ParseInstallation> pushQuery = ParseInstallation
													.getQuery();
											pushQuery
													.whereEqualTo("owner", usr);

											// Send push notification to query
											ParsePush push = new ParsePush();
											push.setQuery(pushQuery); // Set our
																		// Installation
																		// query
											push.setData(data);
											push.sendInBackground();
											/* Show the toast only if the sent
											 * of the request is successful
											 */
											Toast.makeText(
													AddContact.this,
													"Your request has been sent.",
													Toast.LENGTH_SHORT).show();
											Log.d(LOG_TAG,
													"Your request has been sent.");

										} catch (JSONException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
									} else {
										Toast.makeText(AddContact.this,
												"Sorry, you can't add the person with same position.", Toast.LENGTH_LONG)
												.show();
									}
								}
							} else {
								// user doesn't exist in database
								Toast.makeText(AddContact.this,
										"This user doesn't exist",
										Toast.LENGTH_SHORT).show();
							}
						}
					});

					finish();
				} else {
					Log.d(LOG_TAG, name);
					Toast.makeText(AddContact.this,
							"Contact already exists in your list",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Log.e(LOG_TAG, "addNewContact: username length ("
						+ mContactUserNameText.length() + ") is < 0");
				Toast.makeText(AddContact.this,
						"Please type in valid contact username",
						Toast.LENGTH_LONG).show();
			}
		} else {
			Log.d(LOG_TAG, "contact list is full.");
			Toast.makeText(AddContact.this,
					"You can only add up to 3 contacts.", Toast.LENGTH_LONG)
					.show();
		}
	}

	/**
	 * Requested user needs to approve being a friend before communication can occur
	 * @param context
	 * @param username
	 */
	public void approveRequest(final Context context, String username) {

		final ParseContactsManage pFunc = new ParseContactsManage();

		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereEqualTo("username", username);
		Log.d(LOG_TAG, username);

		query.findInBackground(new FindCallback<ParseUser>() {
			public void done(List<ParseUser> objects, ParseException e) {
				if (!objects.isEmpty()) {
					// The query was successful.
					for (ParseUser usr : objects) {

						// adding user that approved
						ArrayList<String> phones = new ArrayList<String>();
						phones.add(usr.getString("phone"));
						pFunc.saveContact(context, ParseUser.getCurrentUser(),
								usr.getUsername(), phones, 1);

						String s = "{\"action\": \"" + ParseActions.approved
								+ "\","
								+ "\"alert\": \"You have been approved by "
								+ ParseUser.getCurrentUser().getUsername()
								+ " !\"," + "\"username\": \""
								+ ParseUser.getCurrentUser().getUsername()
								+ "\"" + "}";

						// send approved notification back to user.
						try {
							JSONObject data = new JSONObject(s);

							// Create our Installation query
							ParseQuery<ParseInstallation> pushQuery = ParseInstallation
									.getQuery();
							pushQuery.whereEqualTo("owner", usr);

							// Send push notification to query
							ParsePush push = new ParsePush();
							push.setQuery(pushQuery); // Set our Installation
														// query
							push.setData(data);
							push.sendInBackground();

						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
				} else {

					Toast.makeText(context, "user not exist",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	/**
	 * Get username without trailing spaces
	 */
	public String retrieveUsername(String contactInfo) {

		String username = "";
		username = contactInfo.trim().split("\n")[0];

		return username;
	}

	public String retrievePhone(String contactInfo) {
		String phone = "";
		String[] list = contactInfo.trim().split("\n");

		if (list.length >= 2) {
			phone = list[1];
		}
		return phone;
	}

	/**
	 * Showing a dialog warning user the contacts list is empty.
	 * 
	 * @param list
	 */
	public void ListEmptyDialog(final Context context) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setMessage("Contacts list is empty.\n"
				+ "Please add contact first.");
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				((Activity) context).finish();
			}
		});
		final AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();

	}
	
	/**
	 * Removes a contact from list
	 */

	@Override
	public void delete(Context context, int position, List<String> contactsList) {
		// TODO Auto-generated method stub
		final ParseContactsManage pFunc = new ParseContactsManage();
		// remove from parse
		pFunc.pDeleteContact(context,
				retrieveUsername(contactsList.get(position)));
		contactsList.remove(position);
	}
}

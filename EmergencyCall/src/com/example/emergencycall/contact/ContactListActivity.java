package com.example.emergencycall.contact;

/**
 * @author: Litao Shen
 * When a user presses and holds on a contact, an option menu displays various
 * options that can be displayed
 */
import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.emergencycall.R;
import com.example.emergencycall.assisted_person.Home_assistedPerson;
import com.example.emergencycall.carer.Home_carer;
import com.example.emergencycall.interfaces.ContactManageInterface;
import com.example.emergencycall.messenger.SendSMS;
import com.parse.ParseUser;


public class ContactListActivity extends ListActivity {

	// TAG for debug
	final String TAG = "ContactListActivity";

	/**
	 * @uml.property name="contactsList"
	 * @uml.associationEnd multiplicity="(0 -1)" elementType="java.lang.String"
	 */
	public static List<String> contactsList = new ArrayList<String>();
	

	ArrayAdapter<String> listAdapter;
	ListView listView;
	
	ContactManageInterface contactManage = new AddContact(); 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setTitle("Contacts");
		
		// Make sure we're running on Honeycomb or higher to use ActionBar APIs
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// Show the Up button in the action bar.
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
		
		listView = getListView();
		listView.setTextFilterEnabled(true);
		

		listAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, contactsList);
		// binding the data to ListView
		setListAdapter(listAdapter);
	    registerForContextMenu(listView);  
	}
	
	
	@Override
	protected void onPause() 
	{
		super.onPause();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		Toast.makeText(this, "Contacts Been Loaded", Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.contact_manage, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	  @Override  
	  public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {  
		  
		  super.onCreateContextMenu(menu, v, menuInfo);

	      super.onCreateContextMenu(menu, v, menuInfo);  
	      
	      	   menu.add(0, v.getId(), 0, "Call");  
	           menu.add(0, v.getId(), 0, "Send Message");  
	           menu.add(0, v.getId(), 0 , "Delete");
	      }  

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		Log.i("Delete", String.valueOf(info.position));
		String name = contactManage.retrieveUsername(contactsList.get(info.position));
		Log.i("Name", name);
		
		String phoneNumber = contactManage.retrievePhone(contactsList.get(info.position));
		Log.i("PhoneNumber", phoneNumber);

		if (item.getTitle().equals("Call")) {
			
			Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
			startActivity(intent);
			
		} else if (item.getTitle().equals("Send Message")) {
			
			Intent intent = new Intent(this, SendSMS.class);
			intent.putExtra("PhoneNumber", phoneNumber);
			startActivity(intent);

		} else if (item.getTitle().equals("Delete")) {
			
			// remove from contactlist array
			contactManage.delete(this, info.position, contactsList);
/*			// remove from parse
			pFunc.pDeleteContact(getApplicationContext(), name);*/
		}
		// notify that contacts list has changed.
		listAdapter.notifyDataSetChanged();
		return true;

	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			String position = ParseUser.getCurrentUser().getString("Position");
			if (position.equals("AssistedPerson")) {
				 startActivity(new Intent(this, Home_assistedPerson.class));
			} else if (position.equals("Carer")) {
				 startActivity(new Intent(this, Home_carer.class));
			}
			finish();
			return true;

		case R.id.action_new:

			Intent intent = new Intent(getApplicationContext(), AddContact.class);
			intent.putExtra("position", "assisted");
			startActivity(intent);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}


}
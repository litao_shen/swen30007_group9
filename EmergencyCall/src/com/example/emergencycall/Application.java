package com.example.emergencycall;

import com.example.emergencycall.messenger.InternetManage;
import com.example.emergencycall.messenger.Messenger;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;
/**
 * @author Litao Shen, Karlen Naidoo
 * 
 * Sets up the Parse SDK and initializes the boolean flag to determine if 
 * we still have Internet connection. Intended to be used for alternative 
 * method to contact Carer  
 *
 */
public class Application extends android.app.Application {
	
	// set flag for Internet connection
	public static boolean internet_state = false;

	public Application() {
	}

	@Override
	public void onCreate() {
		super.onCreate();

		// Initialize the Parse SDK.
		// initialize Parse server
		Parse.initialize(getApplicationContext(), "Q6hRWIru2MXrqrMdwBev8wByHmi3ThIVQ79VorEq", "p0hjnqZn5D5wcjo5kWNxxSP26vfHXeK90T2NXJEu"); 

		// Specify a Activity to handle all pushes by default.
		PushService.setDefaultPushCallback(getApplicationContext(), Messenger.class);

		// Save the current installation.
		ParseInstallation.getCurrentInstallation().saveInBackground();
		
		// check Internet is connected
		internet_state = InternetManage.isNetworkConnected(getApplicationContext());
	}
}
